
function PageFlow2(resources)
{
	PageFlow2.resources = resources;
}
PageFlow2.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 800, Phaser.AUTO, 'PageFlow2', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	//var button;
    //var background;

	preload: function()
	{
		this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 800;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    	
    	this.game.load.image('pageico', PageFlow2.resources.pageico);
    	this.game.load.image('userico', PageFlow2.resources.userico);
    	this.game.load.image('arrow1', PageFlow2.resources.arrow1);

    	this.game.load.image('greydot', PageFlow2.resources.greydot);

    	this.game.load.image('neudot', PageFlow2.resources.neudot);

       this.game.load.spritesheet('button', PageFlow2.resources.pagebtnsprite, 200, 38);

    	this.game.load.image('bckg', PageFlow2.resources.bckg);
    	this.game.stage.backgroundColor = '#000000'

	},

	create: function(evt)
	{
		
     //Background
     this.parent.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
     this.parent.bckg.anchor.set(0.5);
    //break
   
    //this.parent.userico = this.game.add.sprite(200,0, 'userico');

	// User Silhouette Icon
	//this.parent.userico1 = this.game.add.sprite(-200,135, 'userico');
	//this.parent.userico2 = this.game.add.sprite(-200,210, 'greydot');
	//this.parent.userico3 = this.game.add.sprite(-200,280, 'greydot');
	//this.parent.userico4 = this.game.add.sprite(-200,350, 'greydot');
	//this.parent.userico5 = this.game.add.sprite(-200,420, 'greydot');

//Feature Text Description Icon
	/*this.parent.pfield_1 = this.game.add.sprite(200,600, 'neudot');
	this.parent.pfield_2 = this.game.add.sprite(400,600, 'neudot');
	this.parent.pfield_3 = this.game.add.sprite(600,600, 'neudot');
	this.parent.pfield_4 = this.game.add.sprite(200,700, 'neudot');
	this.parent.pfield_5 = this.game.add.sprite(400,700, 'neudot');
	this.parent.pfield_6 = this.game.add.sprite(600,700, 'neudot');*/

	//Paragraph Icon
	//this.parent.paraico1 = this.game.add.sprite(360,100, 'neudot');


	//Header Icon
	this.parent.headerico = this.game.add.sprite(0,0, 'neudot');

	//Arrow pointer
	this.parent.arrow1 = this.game.add.sprite(280,130, 'arrow1');
	//this.parent.button1 = this.game.add.sprite(300,300, 'button');
    // this.parent.button1 = game.add.button(300, 400, 'button', actionOnClick, this, 2, 1, 0);

	//Big Page icon in a circle
    this.parent.pageico = this.game.add.sprite(300,-540, 'pageico');
    this.parent.pageico.anchor.set(0.5);
    //var button;

    //Call the animation build function
    this.parent.buildAnimation();
    
	},

	actionOnClick: function(evt){

if(this.cycleArray[0]){
	   this.game.time.events.remove(this.cycleArray[0]);
}
       console.log("Action Click" + evt.btnId);

       var aAo = evt.btnId;

       this.para_1.setText(this.paraTextArray[aAo]);

       var yLoc = this.arrowArray[aAo] - 2;

       this.tweenArrow = this.game.add.tween(this.arrow1).to( { x:280, y: yLoc }, 1000, Phaser.Easing.Exponential.Out, true);

       this.tweenIcon = this.game.add.tween(this.userico).to( { x:29, y: yLoc }, 30, Phaser.Easing.Exponential.Out, true);
	
	},




	cycleData: function(evt)
			{

		var aAo = 0;	
       //console.log("Action Click" + evt.btnId);
		if(this.countInc <4){
         aAo = this.countInc += 1;
		}else{
		this.countInc = 0;	
		aAo = 0;
		}

       this.para_1.setText(this.paraTextArray[aAo]);

       var yLoc = this.arrowArray[aAo] - 2;

       var tweenArrow = this.game.add.tween(this.arrow1).to( { x:280, y: yLoc }, 1000, Phaser.Easing.Exponential.Out, true);

       var tweenIcon = this.game.add.tween(this.userico).to( { x:29, y: yLoc }, 30, Phaser.Easing.Exponential.Out, true);
	},


	up: function(evt)
			{
			//	debugger
		    //console.log('button up', arguments);
	}
		,
	over: function(evt)
			{
		    //console.log('button over');
	}
		,
	out: function(evt)
			{
		    //console.log('button out');
	}
	,

	createBall: function(evt) {

		console.log("Hello Ball");

    }
    ,

	buildAnimation: function()
	{

		//this.userico.width

		this.countInc = 0;

		this.arrowArray = new Array(134, 204, 275, 345, 415);

	
        ///STYLES

        var style = {font:"bold 24px freight-sans-pro", fill: "#000000", wordWrap: false,wordWrapWidth: 500, align: "left"};

        var styleLeft = {font:"bold 30px freight-sans-pro", fill: "#000000", align: "Left",lineSpacing: -10 };

      
        var iconRight = {font: "32px Arial", fill: "#000000", align: "right"};
        iconRight.align = "left";

          var headerCenter = {font: "64px Arial", fill: "#FFFFFF"};

        //PARAGRAPH TEXT

	    this.paraTextArray = new Array(PageFlow.resources.para1text,PageFlow.resources.para2text,PageFlow.resources.para3text, PageFlow.resources.para4text, PageFlow.resources.para5text);

    	var styleRightMiddle = {font: "32px Arial", fill: "#000000", align: "left", boundsAlignH: "top", boundsAlignV: "right", wordWrap: true, wordWrapWidth: 435 };


   		this.para_1 = this.game.add.text(350, 130, PageFlow.resources.para1text, styleRightMiddle);

        //headerCenter.boundsAlignH ='center']
		///BUILDING BUTTONS IN LOOP

		this.butArray = new Array();

		for (var i=0;i<5;i++){
		var temp = this.game.add.button(30, 135 + (i * 70), 'button', this.actionOnClick, this, 1, 0, 2);
		    temp.btnId = i;
		    temp.alpha = 0;
		    this.butArray.push(temp);
		};

		this.userico = this.game.add.sprite(29,135, 'userico');

		this.userico.alpha = 0;

		
		
		//"headertext":"PAGES",
		//HEADER TEXT ADDED TO SgruntPRITE
		var header_1 = this.game.make.text(0, 0, PageFlow.resources.headertext, headerCenter);
		//field_1.anchor.set(0.5);
		header_1.y = 15; 
		//this.userico1.height-20;
		header_1.x = 250; //this.userico1.width + 20;
		this.headerico.addChild(header_1);

	    this.userico1 = this.game.add.sprite(-200,135, 'neudot');
		this.userico2 = this.game.add.sprite(-200,210, 'neudot');
		this.userico3 = this.game.add.sprite(-200,280, 'neudot');
		this.userico4 = this.game.add.sprite(-200,350, 'neudot');
		this.userico5 = this.game.add.sprite(-200,420, 'neudot');

		//FEILD FOR USER ROLE ICONS

		var field_1 = this.game.make.text(0, 0, PageFlow.resources.ico1text, iconRight);
		field_1.x = this.userico1.width + 20;
		this.userico1.addChild(field_1);

		var field_2 = this.game.make.text(0, 0, PageFlow.resources.ico2text, iconRight);
		field_2.x = this.userico2.width + 20;
		this.userico2.addChild(field_2);

		var field_3 = this.game.make.text(0, 0, PageFlow.resources.ico3text, iconRight);
		field_3.x = this.userico3.width + 20;
		this.userico3.addChild(field_3);

		var field_4 = this.game.make.text(0, 0, PageFlow.resources.ico4text, iconRight);
		field_4.x = this.userico4.width + 20;
		this.userico4.addChild(field_4);

		var field_5 = this.game.make.text(0, 0, PageFlow.resources.ico5text, iconRight);
		field_5.x = this.userico5.width + 20;
		this.userico5.addChild(field_5);

		/////////////////////
		/////////////////////

		this.pfield_1 = this.game.add.text(137, 600, PageFlow.resources.page1text, style);
		this.pfield_2 = this.game.add.text(390, 600, PageFlow.resources.page2text, style);
		this.pfield_3 = this.game.add.text(610, 600, PageFlow.resources.page3text, style);
		this.pfield_4 = this.game.add.text(137, 675, PageFlow.resources.page4text, style);
		this.pfield_5 = this.game.add.text(390, 675, PageFlow.resources.page5text, style);
		this.pfield_6 = this.game.add.text(610, 675, PageFlow.resources.page6text, style);



		this.animate();
	},

	animate: function()
	{

		//ANIMATIONS
	var tweenPageIco = this.game.add.tween(this.pageico).to( { x:300, y: 540 }, 1000, Phaser.Easing.Exponential.Out, true);

	/////////

		var tween1 = this.game.add.tween(this.userico1).to({x: 53, y: 135}, 1000, Phaser.Easing.Exponential.Out, true);

		var tween2 = this.game.add.tween(this.userico2).to({x: 53, y: 208}, 1500, Phaser.Easing.Exponential.Out, true);

		var tween3 = this.game.add.tween(this.userico3).to({x: 53, y: 278}, 2000, Phaser.Easing.Exponential.Out, true);

		var tween4 = this.game.add.tween(this.userico4).to({x: 53, y: 348}, 2500, Phaser.Easing.Exponential.Out, true);

		var tween5 = this.game.add.tween(this.userico5).to({x: 53, y: 418}, 3000, Phaser.Easing.Cubic.Out, true).
                    onComplete.add(function () {
                        console.log('TIMER COMPLETE');
                        this.nextAnim();
                    }, this);
	
	},

	nextAnim: function(evt)
	{
		console.log("DELAYED");

        var tweenUseIcon = this.game.add.tween(this.userico).to( { alpha:1}, 1000, Phaser.Easing.Exponential.In, true);

		var tweenArrow = this.game.add.tween(this.arrow1 ).to( { x:280, y: 130 }, 1000, Phaser.Easing.Exponential.Out, true);

	

		for (var i=0;i<5;i++){
				var temp =  this.butArray[i];
				var tweenTemp = this.game.add.tween(temp).to({alpha: 1}, 500 + (i*500), Phaser.Easing.Exponential.In, true);
		};


        this.cycleArray = new Array();
        this.cycleArray[0] = this.game.time.events.repeat(Phaser.Timer.SECOND * 4, 100, this.cycleData, this);


	},

/////////////////////////////////////////////////

	update: function()
	{

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}